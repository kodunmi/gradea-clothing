import React from 'react'
import './homepage.style.scss'
import DirectoryMenu from '../../components/directory-menu/directory-menu.component'
class HomePage extends React.Component {

    render() {
        return (
            <div className='homepage'>
                <DirectoryMenu />
            </div>
        )
    }
}

export default HomePage;