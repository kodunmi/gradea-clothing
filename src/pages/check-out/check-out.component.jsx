import React from 'react'
import { connect } from 'react-redux'
import {cartItemSelector} from '../../redux/cart/cart.selectors'
import CheckOutItem from '../../components/check-out-item/check-out-item.component'
import {totalCostSelector} from '../../redux/cart/cart.selectors'
import Button from '../../components/button/Button'
import {ReactComponent as EmptyCart} from '../../empty-cart.svg'
import {withRouter} from 'react-router-dom'
import StripeButton from '../../components/stripe/stripeCheckOut.component'
import './check-out.style.scss'

export const CheckOutPage = ({cartItems, totalCost , history}) => {
    
    return (
        <div className="chech-out">
            <h3>Check Out</h3>
            <div className="check-out-table">
                <div className="item-head">
                    <p>Product</p>
                    <p>Description</p>
                    <p>Quantity</p>
                    <p>Price</p>
                    <p>Remove</p>
                </div>
                {cartItems.length > 0 ? cartItems.map( item => <CheckOutItem key={item.id} item={item}/>) : <div className='empty-cart-svg'><EmptyCart/></div> }
                {cartItems.length > 0 ?<h3 className='check-out-totalcost'>TOTAL ${totalCost}</h3> : <div className="text-center"><Button onClick={ () => history.push('/shop')}>Start shopping</Button></div>}

            </div>
            <div style={style}>
                {cartItems.length > 0 ? <StripeButton price={totalCost}/> : ''}
            </div>
        </div>
    )
}

const style = {
    padding : '0px 15% 30px 15%',
    float: 'right',
    clear: 'both'
}
const mapStateToProps = (state) => ({
    cartItems: cartItemSelector(state),
    totalCost: totalCostSelector(state)
})

export default withRouter(connect(mapStateToProps)(CheckOutPage))

