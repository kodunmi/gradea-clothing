import React from 'react'
import {collectionSelector,} from '../../redux/shop/shop.selectors'
import {connect} from 'react-redux'
import CollectionItem from '../../components/collection-item/collection-item.component'
import './collection.style.scss'

const CollectionPage = ({match, item}) => {

    const{ items } = item;
    
    return(
        <div>
            <h2>{item.title}</h2>

            <div className='collection'>
                {
                    items.map(item => <CollectionItem key={item.id} item={item}/>)
                }
            </div>
        </div>
    )
} 

const mapStateToProps = (state , ownProps) =>({
    item : collectionSelector(ownProps)(state)
})
//console.log(mapStateToProp);

export default connect(mapStateToProps)(CollectionPage)