import React from 'react'
import './shop.style.scss'
import  CollectionPreview  from '../../components/collection-preview/collection-preview.component'
import {connect} from 'react-redux'
import {collectionToArraySelector} from '../../redux/shop/shop.selectors'
// import {Route} from 'react-router-dom'
// import CollectionPage from '../collection/collection.component'

const ShopPage = ({ collections}) => {
    console.log(collections);
    
    return (
        <div className='shop-page'>
            <h1>Shope Preview</h1>
            {
                collections.map(({ id, ...orderCollectionItems }) => <CollectionPreview key={id} {...orderCollectionItems} />)
            }

            
        </div>
    )
}
const mapStateToProp = state => ({
    collections : collectionToArraySelector(state)
})
export default connect(mapStateToProp)(ShopPage)