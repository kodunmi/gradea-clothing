import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/database'


// TODO: Replace the following with your app's Firebase project configuration
const firebaseConfig = {
    apiKey: "AIzaSyDZScD8EkGsNdj2rZpxStWIKWvEuzO2xTI",
    authDomain: "gradea-f8221.firebaseapp.com",
    databaseURL: "https://gradea-f8221.firebaseio.com",
    projectId: "gradea-f8221",
    storageBucket: "gradea-f8221.appspot.com",
    messagingSenderId: "595277241857",
    appId: "1:595277241857:web:b94b0c25104cc50c9c6a00",
    measurementId: "G-GS50YD2Q2J"
};

export const createUserProfileDocument = async (userAuth, additionalData) => {
  if(!userAuth) return;
  
  const userRef = firestore.doc(`users/${userAuth.uid}`);
  const getDoc = userRef.get();

  getDoc.then(doc =>  {
    if(!doc.exists){
      const {displayName , email } = userAuth;
      const createdAt = new Date();

      try {
         userRef.set({
          displayName,
          email,
          createdAt,
          ...additionalData
        })
      } catch (error) {
        console.log('error creating user', error);
        
      }
    }
  })
  .catch(err => {
    console.log('error getting document', err);
    
  })

  return userRef;
}


// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export const auth = firebase.auth();
export const firestore = firebase.firestore();
export const database = firebase.database();

const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({
    'login_hint': 'user@example.com',
    'prompt' : 'select_account'
  });

export const signInWithGoogle = () => auth.signInWithPopup(provider).then( result => {
    
}).catch( error =>{
  console.log(error);
  
})

export default firebase