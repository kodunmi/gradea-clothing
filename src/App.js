import React from 'react';
import HomePage from './pages/homepage/homepage.component'
import ShopPage from './pages/shop/shop.component'
import CollectionPage from './pages/collection/collection.component'
import CheckOutPage from './pages/check-out/check-out.component'
import { Route, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import {toggleCartSelector} from './redux/cart/cart.selectors'
import {currentUserSelector} from './redux/user/user.selectors'

import Header from './components/header/Header'
import Cart from './components/cart/cart'
import Logo from './crown.svg'
import Session from './pages/session/session.component'
import { auth, createUserProfileDocument } from './firebase/firebase.utils'
import {setCurrentUser} from './redux/user/user.actions'
const links = [
  {
    name: 'shop',
    url: 'shop',
  },
  {
    name: 'contact',
    url: '',
  },
]

const logo = { image: Logo, url: '/' }


class App extends React.Component {

  unsubscribeFromAuth = null;
 
  componentDidMount() {

    const {setCurrentUser} = this.props;
    
    this.unsubscribeFromAuth = auth.onAuthStateChanged( async (userAuth) => {
        // User is signed in.
      if(userAuth){
        createUserProfileDocument(userAuth).then( userRef => {
          userRef.onSnapshot(userSnapshot => {
            setCurrentUser({
                id: userSnapshot.id,
                ...userSnapshot.data()
              })
                
          }, err => {
            console.log(`Encountered error: ${err}`);
          });
          
      })
      }else{
        setCurrentUser(userAuth)
      }
    });
  }

  componentWillUnmount(){
    this.unsubscribeFromAuth()
  }

  render() {

    const {currentUser, toggleCart} = this.props;
    
    return (
      <div>
        
        <Header logo={logo} links={links} />
        { !toggleCart ? <Cart /> : ''}
        <Route exact path='/' component={HomePage} />
        <Route exact path='/check-out' component={CheckOutPage} />
        <Route exact path='/shop/:collectionId' component={CollectionPage} />
        <Route exact path='/shop' component={ShopPage} />
        <Route exact path='/session' render={() => (currentUser ? (<Redirect to="/"/>) : <Session/>)}/>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  currentUser: currentUserSelector(state),
  toggleCart: toggleCartSelector(state)
})

const mapDispatchToProps = (dispatch) => ({
  setCurrentUser: user => dispatch(setCurrentUser(user))
})

export default connect(mapStateToProps,mapDispatchToProps)(App);
