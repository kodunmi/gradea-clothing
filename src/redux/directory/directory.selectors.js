import {createSelector} from 'reselect';

const stateSelector = state => state;

export const directorySelector = createSelector(
    stateSelector,
    state => state.directory
)