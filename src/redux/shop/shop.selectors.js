import {createSelector} from 'reselect'

const shopSelector = state => state.shop;

export const collectionsSelector = createSelector(
    shopSelector,
    shop => shop.collections
)

export const collectionToArraySelector = createSelector(
    collectionsSelector,
    collections => Object.keys(collections).map(key => collections[key])
)


export const collectionSelector = urlParam => createSelector(    
    [collectionsSelector],
    collections => collections[urlParam.match.params.collectionId]
)