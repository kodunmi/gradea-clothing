export const addItemToCart = (cartItems, itemToAdd) => {
    const existingItem = cartItems.find(cartItem => cartItem.id === itemToAdd.id)

    if(existingItem){
        return cartItems.map(cartItem => cartItem.id === itemToAdd.id ? 
            {...cartItem, quantity: cartItem.quantity + 1}
            : cartItem
        )
    }

    return [...cartItems, {...itemToAdd, quantity: 1}]
}

export const reduceItem = (cartItems, itemToReduce) => {
    if(itemToReduce.quantity > 1){
        return  cartItems.map(cartItem => cartItem.id === itemToReduce.id ?
            {...cartItem, quantity: cartItem.quantity -1}
            :cartItem )
    }
    
    if(itemToReduce.quantity === 1){
       return cartItems.filter(item => item.id !== itemToReduce.id)
    }
}

export const removeItemFromCart = (cartItems, itemToRemove) => {
    return cartItems.filter(item => item.id !== itemToRemove.id )    
}