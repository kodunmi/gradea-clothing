import {cartActionType} from './cart.action.type'

export const addItemToCart = (cartItem) => ({
    type: cartActionType.ADD_ITEM_TO_CART,
    payload:cartItem
})

export const removeItemFromCart = (cartItem) => ({
    type: cartActionType.REMOVE_ITEM_FROM_CART,
    payload: cartItem
})

export const reduceItem = (cartItem) => ({
    type: cartActionType.REDUCE_CART_ITEM,
    payload: cartItem
})

export const toggleCartVisibility = () => ({
    type: cartActionType.TOGGLE_CART,
})
