import {cartActionType} from './cart.action.type'
import {addItemToCart, reduceItem, removeItemFromCart} from './cart.utiles'
const initialState = {
    items:[],
    hidden: true
}

 const cartReducer = (state = initialState, { type, payload }) => {
    switch (type) {

    case cartActionType.ADD_ITEM_TO_CART:
        return { 
            ...state,
            items:addItemToCart(state.items, payload)
        };
    
    case cartActionType.TOGGLE_CART:
        return{
            ...state,
            hidden: !state.hidden
        };
    
    case cartActionType.REDUCE_CART_ITEM:
        return{
            ...state,
            items: reduceItem(state.items, payload)
        };

    case cartActionType.REMOVE_ITEM_FROM_CART:
        return{
            ...state,
            items: removeItemFromCart(state.items, payload)
        };

    default:
        return state
    }
}

export default cartReducer