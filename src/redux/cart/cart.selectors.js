import {createSelector} from 'reselect'


const cartSelector = state => state.cart;

export const cartItemSelector = createSelector(
    cartSelector,
    cart => cart.items
)

export const toggleCartSelector = createSelector(
    cartSelector,
    cart => cart.hidden
)
export const itemsCountSelector = createSelector(
    cartItemSelector,
    items => items.reduce((acc, item) => acc + item.quantity ,0 )
)
export const totalCostSelector = createSelector(
    cartItemSelector,
    items => items.reduce((acc, item) => acc + (item.quantity * item.price) ,0 )
)


