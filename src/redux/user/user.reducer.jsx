import {userActionType} from '../user/user.type'

const initialState = {
    currentUser: null
 }
 
 const userReducer = (state = initialState, { type, payload }) => {
     switch (type) {
 
     case userActionType.SET_CURRENT_USER:
         return { 
            ...state, 
            currentUser: payload 
        }
     default:
         return state
     }
 }

 export default userReducer
 