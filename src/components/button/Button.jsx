import React from 'react'
import './button.style.scss'

const Button = ({children , color, bg , size , opacity, onClick}) => {

    return (
        <button className={`btn ${color} ${size} ${bg}`} style={{opacity: `${opacity}`}} onClick={onClick}>
            {children}
        </button>
    )
}

export default Button
