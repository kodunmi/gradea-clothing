import React from 'react'
import {withRouter, Link} from 'react-router-dom'

import { connect } from 'react-redux'

import {itemsCountSelector} from '../../redux/cart/cart.selectors'
import {currentUserSelector} from '../../redux/user/user.selectors'

import {toggleCartVisibility} from '../../redux/cart/cart.actions'

import Burger from '@animated-burgers/burger-squeeze' 
import '@animated-burgers/burger-squeeze/dist/styles.css' 

import {auth} from '../../firebase/firebase.utils'
import './header.style.scss'

import { ReactComponent as CartIcon } from '../../shopping-bag.svg'

class Header extends React.Component {

    constructor(props) {
        super(props)
    
        this.state = {
             open : false
        }
    }
    
    render(){
        const {history , match , logo, links, user, toggleCart, itemsCount} = this.props
        return (
        <div>
            <div className="header">
                <img src={logo.image} alt="" onClick={() => history.push(`${logo.url}`)} />
                <div className="header-links">
                    {
                        links.map(link => <div key={link.name} className='link' onClick={() => history.push(`${match.url}${link.url}`)}>{link.name}</div>)
                    }
                    {
                        user ? <div className='link' onClick={()=> auth.signOut()}>sign out</div> : <div className='link' onClick={() => history.push('/session')}>account</div>
                    }
                    <div className="cart-logo" onClick={() => toggleCart()}>
                        <CartIcon  className='link'/>
                        <span className='cart-counter'>{itemsCount}</span>
                    </div> 
                </div>
                <div className="menu-icon" onClick={() => this.setState({open: !this.state.open})}>
                    <Burger isOpen={ this.state.open } />
                </div>
            </div>
            {
                this.state.open 
                
                ? 
                
                <div style={style.mobileMenuStyle}>
                    {links.map(link => <p key={link.name}  onClick={() => history.push(`${match.url}${link.url}`)}>{link.name}</p>)}
                    {user ? <p  onClick={()=> auth.signOut()}>sign out</p> : <p className='link' onClick={() => history.push('/session')}>account</p>}
                    <p onClick={() => history.push('/check-out')}>
                        <span>cart</span> <span style={style.cartIcon}>{itemsCount}</span>
                    </p> 
                </div> 

                : ''
            }
            
        </div>
            
            
        )
    }
    
}
const style = {
    mobileMenuStyle: { 
        maxWidth: '100%',
        height: '100%', 
        backgroundColor: 'white', 
        position: 'fixed',
        alignItems: 'center', 
        justifyContent:'center', 
        zIndex: '2' , 
        width: '100%', 
        top: '55px', 
        transition: '1s ease-in-out', 
        fontSize: '40px'
    },
    cartIcon: { 
        padding: '5px 10px', 
        borderRadius: '25%', 
        backgroundColor: 'blue' , 
        color: 'white' 
    }
}

const mapStateToProps = (state) => ({
    user : currentUserSelector(state),
    itemsCount : itemsCountSelector(state)
})

const mapDispatchToProps = (dispatch) => ({
    toggleCart: hidden => dispatch(toggleCartVisibility())
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Header))

  


