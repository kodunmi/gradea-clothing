import React from 'react'
import './check-out-item.style.scss'
import {connect} from 'react-redux'
import {addItemToCart, removeItemFromCart, reduceItem} from '../../redux/cart/cart.actions'


const CheckOutItem = ({item , addItemToCart, removeItemFromCart, reduceItem}) => {
    const {price, quantity, imageUrl , name} = item
    return (
        <div className="check-out-item-wrapper">
            <img className="check-out-item-image" src={imageUrl} alt=""/>
            <p>{name}</p>
            <div className="check-out-item-qty">
                <div className="check-out-item-qty-control remove-btn" onClick={() => reduceItem(item)}>&lt;</div>
                    <p>{quantity}</p>
                <div className="check-out-item-qty-control remove-btn" onClick={() => addItemToCart(item)}>&gt;</div>
            </div>
            <p>{price}</p>
            <div className="remove-btn" onClick={() => removeItemFromCart(item)}>X</div>
        </div>
    )
}

const mapDispatchToProps = dispatch =>({
    addItemToCart: cartItem =>dispatch(addItemToCart(cartItem)),
    removeItemFromCart: cartItem =>dispatch(removeItemFromCart(cartItem)),
    reduceItem: cartItem =>dispatch(reduceItem(cartItem))
})
export default connect(null, mapDispatchToProps)(CheckOutItem)
