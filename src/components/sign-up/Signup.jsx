import React, { Component } from 'react'
import Input from '../input/Input'
import Button from '../button/Button'
import './signup.style.scss'
import { auth, createUserProfileDocument } from '../../firebase/firebase.utils'
export class SignUp extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
             displayName: '',
             email : '',
             password: '',
             comfirmPassword: ''

        }
    }
    
    handleSubmit = async e => {
        e.preventDefault()
        const {displayName, email, password , comfirmPassword} = this.state;

        if(password !== comfirmPassword){
            alert("password does not match")
            return
        }
        
        const {user} = await auth.createUserWithEmailAndPassword(email, password).catch(error=>{
            alert(error.message)
            this.setState({
                email : '',
                password: '',
                comfirmPassword: ''
            })

        }) || {}
        if(user){
            try {
                console.log(user.email)
                await createUserProfileDocument(user, {displayName})
                this.setState({
                    displayName: '',
                    email : '',
                    password: '',
                    comfirmPassword: ''
                })
    
            } catch (error) {
                alert(error);
            }
        }
        

        // auth.createUserWithEmailAndPassword(email, password)
        // .then(user =>{
        //     console.log(user.email)
        //     createUserProfileDocument(user, {displayName})
        // })
        // .catch(err =>{
        //     alert(err.message)
        // })
    }

    handleChange = e => {
        const { name , value} = e.target;
        this.setState({ [name]: value})
    }
    render() {
        return (
            <div className='sign-up'>
                <h1>Create a new account</h1>
                <p>Signup with your email and password</p>
                <form onSubmit={this.handleSubmit}>
                    <Input type='text' label='Full Name' name='displayName' handleChange={this.handleChange} value={this.state.displayName}/>
                    <Input type='email' label='Email' name='email' handleChange={this.handleChange} value={this.state.email}/>
                    <Input type='password' label='password' name='password' handleChange={this.handleChange} value={this.state.password}/>
                    <Input type='password' label='Confirm Password' name='comfirmPassword' handleChange={this.handleChange} value={this.state.comfirmPassword}/>
                    <div style={{ marginTop: '10px'}}>
                        <Button color="white" bg='bg-black' size='lg' opacity='1'>Login</Button>
                    </div>
                </form>
            </div>
        )
    }
}

export default SignUp
