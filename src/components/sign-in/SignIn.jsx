import React, { Component } from 'react'
import './sign-in.style.scss'
import Input from '../input/Input'
import Button from '../button/Button'
import {signInWithGoogle, auth} from '../../firebase/firebase.utils'

export default class SignIn extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             email: '',
             password: '',
        }
    }

    handleSubmit = async e => {

        const {email, password} = this.state;
        e.preventDefault()
        await auth.signInWithEmailAndPassword(email,password)
        .catch(err =>{
            console.log(err.message);
            
        })
        this.setState({email:'', password:''})
    }
    handleChange = e => {
        const { name , value} = e.target;
        this.setState({ [name]: value})
    }

    render() {
        return (
            <div className='sign-in'>
                <h1>I already have an account</h1>
                <p>Signin with your email and password</p>

                <form onSubmit={this.handleSubmit}>
                    <Input name='email' type='text' label='Email' handleChange={this.handleChange} value={this.state.email}/>
                    <Input name='password' type='password' label='Password' handleChange={this.handleChange} value={this.state.password}/>
                    <div style={{ marginTop: '10px', display: 'flex', justifyContent: 'space-between' }}>
                        <Button color="white" bg='bg-black' size='lg' opacity='1'>Login</Button>
                        <Button onClick={signInWithGoogle} color="white" size='lg' opacity='1'>Google</Button>
                    </div>
                   
                </form>
            </div>
        )
    }
}
