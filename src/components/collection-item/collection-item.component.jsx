import React from 'react'
import './collection-item.style.scss'
import {connect} from 'react-redux'
import {addItemToCart} from '../../redux/cart/cart.actions'

const CollectionItem = ({item , addItemToCart}) =>  {
    const { name, imageUrl, price } = item
    
    return (
        <div className='collection-item'>
            <div className='image-container' style={{ backgroundImage: `url(${imageUrl})` }}>
                <div className='cart-btn' onClick={() => addItemToCart(item)}>Add to Cart</div>
            </div>
            <div className='details'>
                <p>{name}</p>
                <p>${price}</p>
            </div>
        </div>
    )
}


const mapDispatchToProps = (dispatch) => ({
    addItemToCart: cartItem =>dispatch(addItemToCart(cartItem))
})


export default connect(null, mapDispatchToProps)(CollectionItem)