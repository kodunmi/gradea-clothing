import React from 'react';
import './input.style.scss'

const Input = ({ type, name , handleChange , value, label}) => {
    return (
        <div className="input">
            <input
                className="input-form"
                type={type}
                name={name}
                onChange={handleChange}
                value={value}
               
            />
            {
                label ?
                    <label className={`label ${value.length > 0 ? 'shrink' : ''}`}>{label}</label> : ''
            }

        </div>
    )
}

export default Input;