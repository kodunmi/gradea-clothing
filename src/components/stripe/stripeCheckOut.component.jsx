import React from 'react'
import StripeCheckout from 'react-stripe-checkout';

const StripeButton = ({price}) => {
    const onToken = (token) => {
        alert('payment successful')
    }
    const amount = price * 100;
    return(
        <StripeCheckout
        token={onToken}
        stripeKey="pk_test_acPN6hcaTuoSrb84COT0WltM"
        name='grade a clothing'
        label='checkout'
        amount={amount}
        shippingAddress
        billingAddress
      />
    )
} 

export default StripeButton