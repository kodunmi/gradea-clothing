import React from 'react'
import './cart-item.style.css'

export const CartItem = ({name, imageUrl, price, quantity}) => {
    return (
        <div className="cart-item">
            <img className="cart-item-image" src={imageUrl} alt=""/>
            <div className="cart-item-detail">
                <p>{name}</p>
                <p>{quantity} x ${price}</p>
            </div>
        </div>
    )
}
