import React from 'react'
import './directory-menu.style.scss'
import  MenuItem  from '../menu-item/menu-item.component'
import {connect} from 'react-redux'
import {directorySelector} from '../../redux/directory/directory.selectors'


const DirectoryMenu =({sections}) => {

    
        return (
            <div className="directory-menu">
                {sections.map(({title, imageUrl, size, id, linkUrl}) => <MenuItem key={id} title={title} img={imageUrl} size={size} linkUrl={linkUrl}/>)}
            </div>
        )
    

};
const mapStateToProp = (state) => ({
    sections : directorySelector(state)
})
export default connect(mapStateToProp)(DirectoryMenu)