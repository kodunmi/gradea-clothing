import React from 'react'
import './cart.style.css'
import {CartItem} from '../cart-item/cart-item.component'
import {cartItemSelector} from '../../redux/cart/cart.selectors'
import Button from '../button/Button'
import {Link} from 'react-router-dom'
import {ReactComponent as EmptyCart} from '../../empty-cart.svg'
import {connect} from 'react-redux'

const Cart = ({items}) => {
    return (
        <div className='cart'>
            {
                items.length > 0 ? items.map(item => <CartItem key={item.id} {...item}/>) : <div style={cartSvg}><EmptyCart/><p style={{ marginBottom: '-20px' }}>Cart Empty</p></div>
            }

            <div className="checkout-btn" >
                <Link to="/check-out"><Button color="black" bg="bg-white" size="lg">Check Out</Button></Link>
            </div>
            
        </div>
    )
}

const cartSvg = {
    textAlign: 'center'
}
const mapStateToProps = state => ({
    items: cartItemSelector(state)
})

export default connect(mapStateToProps)(Cart)

